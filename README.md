# Code

### Libraries
* [kivy](https://kivy.org/#home) - Open source Python library for rapid development of applications
* [pyckitup](https://pickitup247.com/pyckitup.html) - Python game engine for the Web
* [Loguru](https://github.com/Delgan/loguru#readme) - a library which aims to bring enjoyable logging in Python
* [Manim](https://github.com/3b1b/manim) - an animation engine for explanatory math videos
* [Graphene](https://graphene-python.org/) - Graphene-Python is a library for building GraphQL APIs in Python
* [python-stdnum](https://github.com/arthurdejong/python-stdnum) - A Python module to parse, validate and reformat standard numbers and codes in different formats. It contains a large collection of number formats.
* [flexx](https://github.com/flexxui/flexx) - Write desktop and web apps in pure Python
* [cefpython](https://github.com/cztomczak/cefpython) - Python bindings for the Chromium Embedded Framework (CEF)
* [wq.io](https://wq.io/wq.io) - a Pythonic library for consuming (input), iterating over, and generating (output) external data resources in various formats. wq.io facilitates interoperability between the wq framework and other systems and formats.

### Scripts
* [Python automation scripts](https://github.com/avidLearnerInProgress/python-automation-scripts)
* [Python Script for ‘searching’ and ‘downloading’ hundreds of Google images to the local hard disk!](https://pypi.org/project/google_images_download/)

### IDE
* [Spyder - The Scientific PYthon Development EnviRonment](https://pythonhosted.org/spyder/installation.html)
* [PyCharm](https://www.jetbrains.com/pycharm/)
* [Live Coder for VSCode](https://pypi.org/project/live-coder/) - The Server for Live Coder, works with the Live Coder VSCode extension.

### Other
* [iPython](https://en.wikipedia.org/wiki/IPython)<br>
IPython is a command shell for interactive computing in multiple programming languages, originally developed for the Python programming language, that offers introspection, rich media, shell syntax, tab completion, and history. IPython provides the following features:<br>
•	Interactive shells (terminal and Qt-based).<br>
•	A browser-based notebook with support for code, text, mathematical expressions, inline plots and other media.<br>
•	Support for interactive data visualization and use of GUI toolkits.<br>
•	Flexible, embeddable interpreters to load into one's own projects.<br>
•	Tools for parallel computing.<br>
* [PyJS](http://pyjs.org/Overview.html)<br>
Like GWT, pyjs translates a Python application and libraries (including UI widgets and DOM classes) into a JavaScript application and libraries, and packages it all up
* [Jupyter](https://jupyter.org/)
* [Bootle](https://bottlepy.org/docs/dev/)<br>
Bottle is a fast, simple and lightweight WSGI micro web-framework for Python. It is distributed as a single file module and has no dependencies other than the Python Standard Library.
* [Pyramid](https://trypyramid.com/)<br>
Pyramid makes it easy to write web applications. You can start small with this "hello world" minimal request/response web app. This may take you far, especially while learning. As your application grows, Pyramid offers many features that make writing complex software take less effort.
Pyramid works in all supported versions of Python. Our installation instructions will help you get Pyramid up and running.
Pyramid's quick tutorial will take you step by step through writing a single file application, forms, database integration, and authentication.
Developers may dive in to Pyramid's narrative documentation, or browse the extensive API reference. Pyramid has a rich pool of helpful resources from which to draw. Extending Pyramid is a curated and filterable list of add-ons, packages, and applications built to work with Pyramid.
* [TurboGears](http://turbogears.org/)
* [Web2py](http://www.web2py.com/)
* [plone](https://plone.org/)
* [django-cms](https://www.django-cms.org/en/)
* [mezzanine](https://github.com/stephenmcd/mezzanine)
* [Nameko - A microservice framework for Python](https://www.nameko.io/)

##### Resources
* [William Vincent website](https://wsvincent.com/)
* [Corey Schafer website](https://coreyms.com)

### Software
* [qutebrowser - a keyboard-focused browser ](https://qutebrowser.org/)
* [pybrowser](https://github.com/abranjith/pybrowser)

# Flask
[Flask](http://flask.pocoo.org/) is a microframework for Python based on Werkzeug, Jinja 2 and good intentions. And before you ask: It's BSD licensed!
* [Python Flask Tutorial: Full-Featured Web App Part 1 - Getting Started](https://www.youtube.com/watch?v=MwZwr5Tvyxo&feature=youtu.be)
* [Python Flask Tutorial: Full-Featured Web App Part 2 - Templates](https://www.youtube.com/watch?v=QnDWIZuWYW0)
* [Python Flask Tutorial: Full-Featured Web App Part 3 - Forms and User Input](https://www.youtube.com/watch?v=UIJKdCIEXUQ)
* [Python Flask Tutorial: Full-Featured Web App Part 4 - Database with Flask-SQLAlchemy](https://www.youtube.com/watch?v=cYWiDiIUxQc)
* [Python Flask Tutorial: Full-Featured Web App Part 5 - Package Structure](https://www.youtube.com/watch?v=44PvX0Yv368)
* [Python Flask Tutorial: Full-Featured Web App Part 6 - User Authentication](https://www.youtube.com/watch?v=CSHx6eCkmv0)
* [Python Flask Tutorial: Full-Featured Web App Part 7 - User Account and Profile Picture](https://www.youtube.com/watch?v=803Ei2Sq-Zs)
* [Python Flask Tutorial: Full-Featured Web App Part 8 - Create, Update, and Delete Posts](https://www.youtube.com/watch?v=u0oDDZrDz9U)
* [Python Flask Tutorial: Full-Featured Web App Part 9 - Pagination](https://www.youtube.com/watch?v=PSWf2TjTGNY)
* [Python Flask Tutorial: Full-Featured Web App Part 10 - Email and Password Reset](https://www.youtube.com/watch?v=vutyTx7IaAI)
* [Python Flask Tutorial: Full-Featured Web App Part 11 - Blueprints and Configuration](https://www.youtube.com/watch?v=Wfx4YBzg16s)
* [Python Flask Tutorial: Full-Featured Web App Part 12 - Custom Error Pages](https://www.youtube.com/watch?v=uVNfQDohYNI)

# Learning materials 

### Books
* [Making Games with Python & Pygame](http://inventwithpython.com/pygame/)<br>
Making Games with Python & Pygame covers the Pygame library with the source code for 11 games. Making Games was written as a sequel for the same age range as Invent with Python. Once you have an understanding of the basics of Python programming, you can now expand your abilities using the Pygame library to make games with graphics, animation, and sound.
The book features the source code to 11 games. The games are clones of classics such as Nibbles, Tetris, Simon, Bejeweled, Othello, Connect Four, Flood It, and others.
* [Invent Your Own Computer Games with Python, 4th Edition](http://inventwithpython.com/invent4thed/)<br>
Invent Your Own Computer Games with Python teaches you how to program in the Python language. Each chapter gives you the complete source code for a new game, and then teaches the programming concepts from the examples. Games include Guess the Number, Hangman, Tic Tac Toe, and Reversi. This book also has an introduction to making games with 2D graphics using the Pygame framework.
* [Cracking Codes with Python](http://inventwithpython.com/cracking/)<br>
Cracking Codes with Python teaches complete beginners how to program in the Python programming language. The book features the source code to several ciphers and hacking programs for these ciphers. The programs include the Caesar cipher, transposition cipher, simple substitution cipher, multiplicative & affine ciphers, Vigenere cipher, and hacking programs for each of these ciphers. The final chapters cover the modern RSA cipher and public key cryptography.
* [Clean Architectures in Python](https://leanpub.com/clean-architectures-in-python)<br>
What is a good software architecture? Why should we bother structuring the code and spending time testing it? If you like spending hours debugging your programs or staying late at the office to recover from a buggy deploy in production this book is definitely NOT for you!

### Topics
* [What Does It Take To Be An Expert At Python?](https://www.youtube.com/watch?v=7lmCu8wz8ro) | [materials](https://github.com/austin-taylor/code-vault/blob/master/python_expert_notebook.ipynb)
* [Search Algorithms in Python](https://stackabuse.com/search-algorithms-in-python/)
* [Common Gotchas](https://docs.python-guide.org/writing/gotchas/)
* [Python Security Best Practices Cheat Sheet](https://snyk.io/blog/python-security-best-practices-cheat-sheet/)
* [Detecting bots on Reddit](https://www.briannorlander.com/projects/reddit-bot-classifier/)
* [Things you’re probably not using in Python 3 – but should](https://datawhatnow.com/things-you-are-probably-not-using-in-python-3-but-should/)
* [Practice problems for interviews](https://github.com/devAmoghS/Practice-Problems)
* [PL - Generatory, funkcje generujące](https://chyla.org/blog/Python_-_Generatory/)

##### Web scraping
* [Extract text from a webpage using BeautifulSoup and Python](https://matix.io/extract-text-from-webpage-using-beautifulsoup-and-python/)
* [Scraping Top 50 Movies on IMDb using BeautifulSoup, Python](https://medium.com/@nishantsahoo/which-movie-should-i-watch-5c83a3c0f5b1)
* [HTML Scraping](https://docs.python-guide.org/scenarios/scrape/)
* [Python Tutorial: Web Scraping with BeautifulSoup and Requests](https://www.youtube.com/watch?v=ng2o98k983k&feature=youtu.be)
* [Learning Python Requests In ONE VIDEO](https://www.youtube.com/watch?v=KnBntO0wayk&feature=youtu.be)

### Video tutorials
* [Python Tutorial - Create a EMAIL Bomb/Bomber](https://www.youtube.com/watch?v=9R0Zg3MCpUo&feature=youtu.be)
* [Python in 6 hours](https://www.youtube.com/watch?v=_uQrJ0TkZlc)
* [Python programming tutorials](https://www.youtube.com/channel/UCxPWtz5N--X3IyYJ13Zr99A/videos)
* [Create your own Simple 2D GAME using Python (Part 1) - Map Creation](https://www.youtube.com/watch?v=Li7LskHpL20&t=)
* [Building a keylogger using Python + Pynput](https://www.youtube.com/playlist?list=PLhTjy8cBISEoYoJd-zR8EV0NqDddAjK3m)
* [Build a Data Analysis Library from Scratch in Python](https://www.youtube.com/playlist?list=PLVyhfExBT1XDTu-oocI3ttl_OPhulAJOp)

### Tutorials
* [Python REST APIs + Flask + Angular CRUD Example](https://www.roytuts.com/python-rest-flask-angularjs-crud/)
* [8 new anti-patterns for Python](https://deepsource.io/blog/8-new-python-antipatterns/)
* [Advanced NLP with spaCy](https://course.spacy.io/)
* [Comprehensive Python Cheatsheet](https://github.com/gto76/python-cheatsheet)
* [Python Basics](https://github.com/learnbyexample/Python_Basics)
* [Tkinter Python Tutorial](https://morioh.com/p/b09f4da41907/tkinter-python-tutorial-python-gui-programming-using-tkinter-tutorial-python-training)
* [Python F String - The Ultimate Usage Guide](https://saralgyaan.com/posts/f-string-in-python-usage-guide/)
* [240+ Python Tutorials - Learn Python from Scratch](https://data-flair.training/blogs/python-tutorials-home/)

### PyGame
* [PyGame: A Primer on Game Programming in Python](https://realpython.com/pygame-a-primer/)

### Machine Learning
* [Applied Deep Learning with PyTorch](https://morioh.com/p/433f0ddf1da6/applied-deep-learning-with-pytorch-full-course)
* [A Complete Machine Learning Project Walk-Through in Python (Part One): Putting the machine learning pieces together](https://morioh.com/p/b56ae6b04ffc/a-complete-machine-learning-project-walk-through-in-python)
* [Introduction to K-Means Clustering in Python with scikit-learn](https://blog.floydhub.com/introduction-to-k-means-clustering-in-python-with-scikit-learn/)
* [10 Amazing Articles On Python Programming And Machine Learning](https://hackernoon.com/10-great-articles-on-python-development-6f54dd38437f)
* [Detecting Fake News](https://data-flair.training/blogs/advanced-python-project-detecting-fake-news/)

### Var
* [Python Functions For Go](https://github.com/MeteHanC/python-functions-for-go)

### Websites
* [Al Sweigart twitch website](https://www.twitch.tv/alsweigart)